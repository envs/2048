/* 
 * _  )   \  | |  _ )
 *   /  (  |__ _| _ \
 * ___|\__/   _|\___/
 *
 * original game designed by Gabriele Cirulli
 * 
 * This version:
 *
 * Released under the Floodgap Free Software License
 * https://www.floodgap.com/software/ffsl/license.txt
 *
 * (c) 2020 Brian Evans < sloum AT rawtext.club >,
 * All Rights Reserved
 *
 */

 
#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

enum editorKey {
  ARROW_LEFT = 1000,
  ARROW_RIGHT,
  ARROW_UP,
  ARROW_DOWN,
};

int board[4][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
int score = 0;
int moved = 0;
struct termios orig_termios;

void die(const char *s) {
  write(STDOUT_FILENO, "\033[m", 3);
  write(STDOUT_FILENO, "\033[2J", 4);
  write(STDOUT_FILENO, "\033[H", 3);
  write(STDERR_FILENO, "\033[?25h", 6);
  perror(s);
  write(STDOUT_FILENO, "\n", 1);
  exit(1);
}

void quit() {
  write(STDOUT_FILENO, "\033[m", 3);
  write(STDOUT_FILENO, "\033[2J", 4);
  write(STDOUT_FILENO, "\033[H", 3);
  write(STDERR_FILENO, "\033[?25h", 6);
  exit(0);
}

void disableRawMode() {
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios) == -1) 
    die("tcsetattr");
  write(STDERR_FILENO, "\033[?25h", 6);
}

void enableRawMode() {
  if (tcgetattr(STDIN_FILENO, &orig_termios) == -1) die("tcgetattr");
  atexit(disableRawMode);

  struct termios raw = orig_termios;
  raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
  raw.c_cflag |= ~(CS8);
  raw.c_lflag &= ~(ECHO | ICANON | IEXTEN );

  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1) die("tcsetattr");
  write(STDERR_FILENO, "\033[?25l", 6);
}

// This is probably overlong and complex for the needs of this
// game, but should handle clearing out multibyte keys/chords
// so that remnants of them do not get picked up as actual valid
// commands.
int readKey() {
  int nread;
  char c;
  while ((nread = read(STDIN_FILENO, &c, 1)) != 1) {
    if (nread == -1 && errno != EAGAIN) die("read");
  }

  if (c == '\033') {
    char seq[3];

    if (read(STDIN_FILENO, &seq[0], 1) != 1) return '\033';
    if (read(STDIN_FILENO, &seq[1], 1) != 1) return '\033';
    if (seq[0] == '[') {
      if (seq[1] >= '0' && seq[1] <= '9') {
        return '\033';
      } else {
        switch (seq[1]) {
          case 'A': return ARROW_UP;
          case 'B': return ARROW_DOWN;
          case 'C': return ARROW_RIGHT;
          case 'D': return ARROW_LEFT;
          default: return '\033';
        }
      }
    } else if (seq[0] == 'O') {
      return '\033';
    }
    return '\033';
  }
  return c;
}

// The slide functions likely could have been
// written as one function, but I felt that it
// would be less readable and in such a small
// program the bit of code duplication was
// acceptable.
void slideUp() {
  for (int y = 1; y < 4; y++) {
    for (int x = 0; x < 4; x++) {
      if (board[y][x]) {
        int p = y - 1;
        while (p >= 0 && !board[p][x]) p--; 
        if (p < 0) p = 0;
        if (!board[p][x]) {
          board[p][x] = board[y][x];
          board[y][x] = 0;
          moved++;
        } else if (board[p][x] == board[y][x]) {
          board[p][x] = board[p][x] * 2 * -1;
          score += abs(board[p][x]);
          board[y][x] = 0;
          moved++;
        } else if (p < 3 && p != y - 1) {
          board[p+1][x] = board[y][x];
          board[y][x] = 0;
          moved++;
        }
      }
    }
  }
}

void slideDown() {
  for (int y = 2; y > -1; y--) {
    for (int x = 0; x < 4; x++) {
      if (board[y][x]) {
        int p = y + 1;
        while (p <= 3 && !board[p][x]) p++; 
        if (p > 3) p = 3;
        if (!board[p][x]) {
          board[p][x] = board[y][x];
          board[y][x] = 0;
          moved++;
        } else if (board[p][x] == board[y][x]) {
          board[p][x] = board[p][x] * 2 * -1;
          score += abs(board[p][x]);
          board[y][x] = 0;
          moved++;
        } else if (p > 0 && p != y + 1) {
          board[p-1][x] = board[y][x];
          board[y][x] = 0;
          moved++;
        }
      }
    }
  }
}

void slideRight() {
  for (int y = 0; y < 4; y++) {
    for (int x = 2; x > -1; x--) {
      if (board[y][x]) {
        int p = x + 1;
        while (p <= 3 && !board[y][p]) p++; 
        if (p > 3) p = 3;
        if (!board[y][p]) {
          board[y][p] = board[y][x];
          board[y][x] = 0;
          moved++;
        } else if (board[y][p] == board[y][x]) {
          board[y][p] = board[y][p] * 2 * -1;
          score += abs(board[y][p]);
          board[y][x] = 0;
          moved++;
        } else if (p > 0 && p != x + 1) {
          board[y][p-1] = board[y][x];
          board[y][x] = 0;
          moved++;
        }
      }
    }
  }
}

void slideLeft() {
  for (int y = 0; y < 4; y++) {
    for (int x = 1; x < 4; x++) {
      if (board[y][x]) {
        int p = x - 1;
        while (p >= 0 && !board[y][p]) p--; 
        if (p < 0) p = 0;
        if (!board[y][p]) {
          board[y][p] = board[y][x];
          board[y][x] = 0;
          moved++;
        } else if (board[y][p] == board[y][x]) {
          board[y][p] = board[y][p] * 2 * -1;
          score += abs(board[y][p]);
          board[y][x] = 0;
          moved++;
        } else if (p < 3 && p != x - 1) {
          board[y][p+1] = board[y][x];
          board[y][x] = 0;
          moved++;
        }
      }
    }
  }
}

void addRandomTile() {
  int num = rand() % 2;
  int spaces = 0;
  num = num == 0 ? 2 : 4;
  for (int y = 0; y < 4; y++) {
    for (int x = 0; x < 4; x++) {
      if (board[y][x] == 0) spaces++;
    }
  }
  int x, y;
  while (spaces) {
    x = rand() % 4;
    y = rand() % 4;
    if (board[y][x] == 0) {
      board[y][x] = num;
      break;
    }
  }
}

void printCenteredSquare(int n) {
  char a[5];
  sprintf(a, "%d", n);
  int before = (6 - strlen(a)) / 2;
  int after = 6 - before - strlen(a);
  printf("%*s%s%*s|", before, "", n ? a : " ", after, "");
}

void drawBoard() {
  write(STDOUT_FILENO, "\033[m", 3);
  write(STDOUT_FILENO, "\033[2J", 4);
  write(STDOUT_FILENO, "\033[H", 3);
  printf("      _  )   \\  | |  _ )\n");
  printf("        /  (  |__ _| _ \\\n");
  printf("      ___|\\__/   _|\\___/\n");
  printf("+%6s+%6s+%6s+%6s+\n","--  ", "--  ", "--  ", "--  ");
  for (int y = 0; y < 4; y++) {
    printf("|%6s|%6s|%6s|%6s|\n","", "", "", "");
    printf("|");
    for (int x = 0; x < 4; x++) {
      board[y][x] = abs(board[y][x]);
      printCenteredSquare(board[y][x]);
    }
    printf("\n|%6s|%6s|%6s|%6s|\n","", "", "", "");
    printf("+%6s+%6s+%6s+%6s+\n","--  ", "--  ", "--  ", "--  ");
  }
  score = abs(score);
  printf("\nSCORE: %d\n", score);
}

void processKeypress() {
  int c = readKey();
  switch (c) {
    case ARROW_UP: 
    case 'k':
    case 'w':
      slideUp();
      break;
    case ARROW_DOWN:
    case 'j':
    case 's':
      slideDown();
      break;
    case ARROW_LEFT:
    case 'h':
    case 'a':
      slideLeft();
      break;
    case ARROW_RIGHT:
    case 'l':
    case 'd':
      slideRight();
      break;
    case 'q':
      quit();
      break;
  }
}

int alive() {
  for (int y = 0; y < 4; y++) {
    for (int x = 0; x < 4; x++) {
      if (board[y][x] == 0 || 
          (x < 3 && board[y][x] == board[y][x+1]) || 
          (y < 3 && board[y][x] == board[y+1][x])) 
        return 1;
    }
  }
  return 0;
}

int main() {
  time_t t;
  srand((unsigned) time(&t));
  addRandomTile();
  enableRawMode();
  while (1) {
    moved = 0;
    drawBoard();
    processKeypress();
    if (!alive()) break;
    if (moved) addRandomTile();
  };
  printf("Game over!!\n");
  return 0;
}
