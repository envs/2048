BINARY := 2048
PREFIX := /usr/local
EXEC_PREFIX := ${PREFIX}
BINDIR := ${PREFIX}/bin
DATAROOTDIR := ${PREFIX}/share
MANDIR := ${DATAROOTDIR}/man
MAN1DIR := ${MANDIR}/man1

2048: 2048.c
	$(CC) 2048.c -o ${BINARY} -Wall -Wextra -pedantic -std=c99

.PHONY: install
install: clean install-bin install-man clean

.PHONY: install-bin
install-bin: 2048
	install -d ${BINDIR}
	install -m 0755 ./${BINARY} ${BINDIR}

.PHONY: install-man
install-man: 2048.1
	gzip -k ./${BINARY}.1
	install -d ${MAN1DIR}
	install -m 0644 ./${BINARY}.1.gz ${MAN1DIR}

.PHONY: clean
clean:
	rm -f ./${BINARY}
	rm -f ./${BINARY}.1.gz

.PHONY: uninstall
uninstall: clean
	rm -f ${MAN1DIR}/${BINARY}.1.gz
	rm -f ${BINDIR}/${BINARY}

