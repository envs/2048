# 2048

According to wikipedia:
> "2048 is a single-player sliding block puzzle game designed by Italian web developer Gabriele Cirulli. The game's objective is to slide numbered tiles on a grid to combine them to create a tile with the number 2048."

This version of 2048 is implemented in C as a CLI game. Unlike the original, there is not an end to the game (it does not end at 2048) in order to allow players to try for ever higher scores.

![A screenshot of 2048 being played](2048.png)

## How to play

The game takes place on a 4 x 4 grid. Each turn a 2 or a 4 is placed into a random empty square. The player then slides all numbers in an orthagonal direction. If two numbers that match slide into each other they are combined into one square of double the value of the original value int he square. This combining _does not_ compound. So if you had: `- 2 2 4` and you slid left you would end up with `4 4 - -` and _not_ `8 - - -`. Play continues until it is no longer possible to slide in any direction and have a change in board state (moved or combined numbers).

In the original game the goal was to get a tile valued at 2048. This is no easy task. Nonetheless, in this version the game will continue and allow players to amass still higher scores.

## Controls

Arrows keys, vi style movement keys, and asdw all work to slide the pieces, q quits:
- a, h, or left arrow: slide left
- d, l, or right arrow: slide right
- w, k, or up arrow: slide up
- s, j, or down arrow: slide down
- q: quit

## Requirements

To build 2048 you will need a C compiler. The included makefile should work for most folks. If you have issues it may be that some flags may not be valid for your compiler and these can be changed out in the makefile or compilation can be done manually.

For most people it should be as simple as:

```shell
make && ./2048
```

or to install for the system (may require sudo):

```shell
make install && 2048
```

## Hacking

The game should be easy to edit and take apart. It is a few hundred lines of C that could _definitely_ be condensed to less. As a result of lazy coding a few decisions were made that resulted in some level of code duplication (but possibly make it easier to read). Happy hacking to any that feel like it.

## License

As has become a recent habit of mine, this software is released under the [Floodgap Free Software License](https://www.floodgap.com/software/ffsl/license.txt). A license file is included with the code and a notice is included in the source itself. For those not familiar: the FFSL is a GPL non-compatible license. Many would call it a non-free license (from the FSF and OSS definitions). The basics are that you can use this software however you want so long as you do not charge for it and that derivitives cannot be sold. It _can_ be bundled in with other software that is charged for, so long as the person doing the selling makes this piece of software freely available without charge as well. Otherwise, have at it.

